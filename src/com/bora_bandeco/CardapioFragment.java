package com.bora_bandeco;

import java.text.SimpleDateFormat;

import com.bora_bandeco.dto.Cardapio;
import com.bora_bandeco.R;
import com.google.gson.Gson;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
	
public class CardapioFragment extends Fragment {
	
	Cardapio cardapio;
	 
    static CardapioFragment init(String jsonCardapio) {
    	CardapioFragment cardapioFrag = new CardapioFragment();
        
    	// Supply val input as an argument.
        Bundle args = new Bundle();
        args.putString(AppConstants.CARDAPIO_JSON, jsonCardapio);
        cardapioFrag.setArguments(args);
        return cardapioFrag;
    }
	
	public CardapioFragment() {
		super();
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (getArguments() != null) {
        	String json = getArguments().getString(AppConstants.CARDAPIO_JSON);
        	cardapio = (Cardapio) new Gson().fromJson(json, Cardapio.class);
        } else {
        	cardapio = null;
        }
    }
    
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_cardapio, container, false);
        
		if (cardapio != null) {
			exibirCardapio(cardapio, rootView);
		}
		
        return rootView;
    }
	
	private void exibirCardapio(Cardapio cardapio, ViewGroup rootView) {
		TextView v = (TextView)rootView.findViewById(R.id.tipo);
		if (cardapio.tipo.equals(Cardapio.ALMOCO)) {
			v.setText(Html.fromHtml("<b>"+getString(R.string.almoco)+"</b>"));
		} else {
			v.setText(Html.fromHtml("<b>"+getString(R.string.jantar)+"</b>"));
		}
		
		v = (TextView)rootView.findViewById(R.id.data);
		String data = new SimpleDateFormat("dd/MM/yyyy").format(cardapio.getData());
		v.setText(Html.fromHtml("<b>" + cardapio.getDiaSemana() + "&nbsp;(" +data+")</b>"));
		
		v = (TextView)rootView.findViewById(R.id.prato_principal);
		v.setText(Html.fromHtml("<b>"+getString(R.string.prato_principal)+":&nbsp;&nbsp;</b>"+cardapio.prato_principal));
		
		v = (TextView)rootView.findViewById(R.id.guarnicao);
		if (cardapio.guarnicao.equals("")) {
			v.setVisibility(View.GONE);
		} else {
			v.setText(Html.fromHtml("<b>"+getString(R.string.guarnicao)+":&nbsp;&nbsp;</b>"+cardapio.guarnicao));			
		}
		
		
		v = (TextView)rootView.findViewById(R.id.pts);
		if (cardapio.pts.equals("")) {
			v.setVisibility(View.GONE);
		} else {
			v.setText(Html.fromHtml("<b>"+getString(R.string.pts)+":&nbsp;&nbsp;</b>"+cardapio.pts));
		}
		
		
		v = (TextView)rootView.findViewById(R.id.salada);
		v.setText(Html.fromHtml("<b>"+getString(R.string.salada)+":&nbsp;&nbsp;</b>"+cardapio.salada));
		
		v = (TextView)rootView.findViewById(R.id.sobremesa);
		v.setText(Html.fromHtml("<b>"+getString(R.string.sobremesa)+":&nbsp;&nbsp;</b>"+cardapio.sobremesa));
		
		v = (TextView)rootView.findViewById(R.id.suco);
		v.setText(Html.fromHtml("<b>"+getString(R.string.suco)+":&nbsp;&nbsp;</b>"+cardapio.suco));
		
		v = (TextView)rootView.findViewById(R.id.vegetariano);
		if (cardapio.vegetariano.equals("")) {
			v.setVisibility(View.GONE);
		} else {
			v.setText(Html.fromHtml("<b>"+getString(R.string.vegetariano)+":&nbsp;&nbsp;</b>"+cardapio.vegetariano));
		}
	}
}
