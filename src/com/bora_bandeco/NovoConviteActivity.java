package com.bora_bandeco;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class NovoConviteActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_novo_convite);
		
		final Button button = (Button) findViewById(R.id.selecionar_convidados);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(),
        				SelecionarConvidadosActivity.class);
            	
            	SharedPreferences settings = getSharedPreferences(AppConstants.PREFERENCES, 0);
        		String json = settings.getString(AppConstants.CONTATOS_JSON, AppConstants.JSON_VAZIO);
            	intent.putExtra(AppConstants.CONTATOS_JSON, json);
        		startActivity(intent);
            }
        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			default:
				break;
	    }
	    
	    return true;
	}
}
