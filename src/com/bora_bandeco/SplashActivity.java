package com.bora_bandeco;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;

import com.bora_bandeco.request.RequestControllerTask;
import com.bora_bandeco.request.RequestTask;

public class SplashActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
	
		inicializaCardapio();
	}

	private void inicializaCardapio() {
		SharedPreferences settings = getSharedPreferences(AppConstants.PREFERENCES, 0);
		long ultimaAtualizacaoCardapio = settings.getLong(AppConstants.ULTIMA_ATUALIZACAO_CARDAPIO, 0);
		
		Calendar cal = new GregorianCalendar();
		cal = new GregorianCalendar(cal.get(Calendar.YEAR), 
		        cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) {
		    cal.add(Calendar.DAY_OF_WEEK, -1);
		}
		long ultimoSabado = cal.getTimeInMillis();
		
		//Se a última atualização ocorreu antes do último sábado, recuperar o cardápio desta semana
		if (ultimaAtualizacaoCardapio < ultimoSabado || internetEstaDisponivel()) {
			new RequestControllerTask(SplashActivity.this).execute(
						RequestControllerTask.URL_GET_DATA,
						RequestTask.METHOD_GET);
		} else {
			String cardapioDaSemana = settings.getString(AppConstants.CARDAPIO_JSON, AppConstants.JSON_VAZIO);
			
			Intent intent = new Intent(getApplicationContext(),
					CardapioActivity.class);
			intent.putExtra(AppConstants.CARDAPIO_JSON, cardapioDaSemana);
			startActivity(intent);
			finish();
		}		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}
	
	private boolean internetEstaDisponivel() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

}
