package com.bora_bandeco.request;

public enum RequestStatusEnum {

	/** The request was successfully. */
	OK(0),

	/** The request returned an error. */
	ERROR(1),

	/** Occurred a connection error in the request. */
	CONNECTION_ERROR(3);

	/** The code. */
	private int code;

	/**
	 * Instantiates a new code enum.
	 * 
	 * @param code
	 *            the code
	 */
	RequestStatusEnum(int code) {
		this.code = code;
	}

	/**
	 * Code.
	 * 
	 * @return the int
	 */
	public int code() {
		return this.code;
	}

}
