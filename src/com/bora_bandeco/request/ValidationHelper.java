package com.bora_bandeco.request;

import java.util.Collection;
import java.util.Map;

public class ValidationHelper {

	/**
	 * Checks if the string is empty or void.
	 * 
	 * @param string
	 *            the string
	 * @return true, if is empty or void
	 */
	public static boolean isEmptyOrVoid(final String string) {
		if (string == null || string.length() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the string is null or zero.
	 * 
	 * @param Integer
	 *            the integer
	 * @return true, if is null or zero
	 */
	public static boolean isNullOrZero(final Integer integer) {
		if (integer == null || integer.intValue() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the collection is empty or void.
	 * 
	 * @param list
	 *            the list
	 * @return true, if is empty or void
	 */
	public static boolean isEmptyOrVoid(Collection<?> list) {
		if (list == null || list.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the map is empty or void.
	 * 
	 * @param list
	 *            the list
	 * @return true, if is empty or void
	 */
	public static boolean isEmptyOrVoid(Map<?, ?> list) {
		if (list == null || list.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the validateString matches the regExp.
	 * 
	 * @param validateString
	 *            the validate string
	 * @param regExp
	 *            the reg exp to do the validation
	 * @return true, if is valid
	 */
	public static boolean isValid(String validateString, String regExp) {
		if (isEmptyOrVoid(validateString)) {
			return false;
		} else if (validateString.matches(regExp)) {
			return true;
		}
		return false;
	}

	public static boolean isValidCellPhone(String phoneAc, String phoneNb) {

		if (isEmptyOrVoid(phoneAc) || isEmptyOrVoid(phoneNb)) {
			return false;
		} else if (phoneAc.equals(ValidationConstants.PHONE_AREACODE_SP)) {
			if (phoneNb.matches(ValidationConstants.PHONENUMBER_CELL_SP)) {
				return true;
			}
		} else if (phoneNb.matches(ValidationConstants.PHONENUMBER_CELL)) {
			return true;
		}

		return false;
	}

	public static boolean isValidPhoneNumber(String phoneAc, String phoneNb) {

		if (isEmptyOrVoid(phoneAc) || isEmptyOrVoid(phoneNb)) {
			return false;
		} else if (phoneAc.equals(ValidationConstants.PHONE_AREACODE_SP)) {
			if (phoneNb.matches(ValidationConstants.PHONENUMBER_ANY_SP)) {
				return true;
			}
		} else if (phoneNb.matches(ValidationConstants.PHONENUMBER_ANY)) {
			return true;
		}

		return false;
	}
}
