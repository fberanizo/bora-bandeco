package com.bora_bandeco;

import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ContatoAdapter extends ArrayAdapter<ItemContato> {
	private List<ItemContato> contatos;
	private final Activity context;
	
	public ContatoAdapter(Activity context, List<ItemContato> contatos) {
		super(context, R.layout.contato_view, contatos);
		this.context = context;
		this.contatos = contatos;
	}
	
	static class ViewHolder {
	    public TextView text;
	    public ImageView image;
	    protected CheckBox checkbox;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
	    if (convertView == null) {
	      LayoutInflater inflator = context.getLayoutInflater();
	      view = inflator.inflate(R.layout.contato_view, null);
	      final ViewHolder viewHolder = new ViewHolder();
	      viewHolder.text = (TextView) view.findViewById(R.id.nome_contato);
	      viewHolder.image = (ImageView) view.findViewById(R.id.foto_contato);
	      viewHolder.checkbox = (CheckBox) view.findViewById(R.id.check);
	      viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

	            @Override
	            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	            	ItemContato element = (ItemContato) viewHolder.checkbox.getTag();
	            	element.selecionado = buttonView.isChecked();
	            }
	          });
	      view.setTag(viewHolder);
	      viewHolder.checkbox.setTag(contatos.get(position));
	    } else {
	      view = convertView;
	      ((ViewHolder) view.getTag()).checkbox.setTag(contatos.get(position));
	    }
	    
	    ViewHolder holder = (ViewHolder) view.getTag();
	    holder.text.setText(contatos.get(position).usuario.nome);
	    Bitmap foto = contatos.get(position).usuario.getFoto();
	    if (foto != null) {
	    	holder.image.setImageBitmap(foto);
	    } else {
	    	holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_person));
	    }
	    holder.checkbox.setChecked(contatos.get(position).selecionado);
	    return view;
    }
}
