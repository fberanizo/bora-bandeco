package com.bora_bandeco.dto;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

public class Usuario {

	public String nome;
	public String numero_telefone;
	public String foto;
	public String gcm_regid;
	
	public Bitmap getFoto() {
		if (!foto.equals("")) {
			byte[] decodedString = Base64.decode(foto, Base64.DEFAULT);
			return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
		}
		return null;
	}
}