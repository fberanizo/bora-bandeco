package com.bora_bandeco.request;

public class ValidationConstants {
	/** The Constant NUMBER (Long or Integer). */
	public static final String NUMBER = "[0-9]+";

	/** The Constant TOKEN 8 digits. */
	public static final String TOKEN = "[0-9]{8}";
	
	public static final String ANY = ".+";
	
	// PHONE VALIDATION {
	/** The Constant PHONENUMBER. */
	public static final String PHONENUMBER = "\\d{8,9}";
	
	/** The Constant PHONE_AREACODE_SP. */
	public static final String PHONE_AREACODE_SP = "11";
	
	/** The Constant PHONENUMBER_CELL_SP. */
	public static final String PHONENUMBER_CELL_SP = "(5|6|7|8|9)\\d{7,8}";
	
	/** The Constant PHONECODE. */
	public static final String PHONECODE = "\\d{2,3}";
	
	/** The Constant CELLPHONENUMBER. */
	public static final String PHONENUMBER_CELL = "(5|6|7|8|9)\\d{7}";
	
	/** The Constant PHONENUMBER_ANY_SP. */
	public static final String PHONENUMBER_ANY_SP = "\\d{8,9}";
	
	/** The Constant ANYPHONENB. */
	public static final String PHONENUMBER_ANY = "\\d{8}";
	// }

}
