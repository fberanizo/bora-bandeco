package com.bora_bandeco;

public class AppConstants {
	/** Arquivo onde são guardadas as SharedPreferences. */
	public static final String PREFERENCES = "com.bora_bandeco.PREFERENCES";
	
	public static final String JSON_VAZIO = "{}";
	
	public static final String ULTIMA_ATUALIZACAO_CARDAPIO  = "com.bora_bandeco.ULTIMA_ATUALIZACAO_CARDAPIO";
	
	public static final String CARDAPIO_JSON = "com.bora_bandeco.CARDAPIO_JSON";
	
	public static final String USUARIO_JSON = "com.bora_bandeco.USUARIO_JSON";
	
	public static final String CONTATOS_JSON = "com.bora_bandeco.CONTATOS_JSON";
	
	public static final String SENDER_ID = "599502197587";
}
