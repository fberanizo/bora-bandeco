package com.bora_bandeco;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.bora_bandeco.dto.ListaContatos;
import com.bora_bandeco.dto.Usuario;
import com.google.gson.Gson;

public class SelecionarConvidadosActivity extends ActionBarActivity {
	ListView listView;
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selecionar_convidados);
		
		String json = getIntent().getExtras().getString(AppConstants.CONTATOS_JSON);
		ListaContatos contatos = (ListaContatos) new Gson().fromJson(json, ListaContatos.class);
		
		List<ItemContato> list = new ArrayList<ItemContato>();
		
		if (contatos.usuarios != null) {
			for (Usuario u : contatos.usuarios) {
				list.add(new ItemContato(u));
			}
		}
		
		ContatoAdapter adapter = new ContatoAdapter(this, list);
		listView = (ListView) findViewById(R.id.lista_contatos);
		listView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.selecionar_convidados, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.enviar_convite:
		
			    break;
		    
	    }
	    
	    return true;
	}

}
