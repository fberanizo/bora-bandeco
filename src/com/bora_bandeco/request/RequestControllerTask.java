package com.bora_bandeco.request;

import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.bora_bandeco.AppConstants;
import com.bora_bandeco.CardapioActivity;
import com.bora_bandeco.R;

public class RequestControllerTask extends RequestTask {

	public static final String URL_GET_DATA = "http://www.students.ic.unicamp.br/~ra104864/bora_bandeco/api/cardapio_da_semana.php";

	private Activity mActivity;
	private ProgressDialog pd;

	public RequestControllerTask(Activity mActivity) {
		super();
		this.mActivity = mActivity;
		this.pd = ProgressDialog.show(mActivity, "",
				mActivity.getString(R.string.carregando), true);
	}

	@Override
	protected void onPostExecute(RequestDTO result) {
		SharedPreferences settings = mActivity.getSharedPreferences(AppConstants.PREFERENCES, 0);
		String cardapioDaSemana = settings.getString(AppConstants.CARDAPIO_JSON, AppConstants.JSON_VAZIO); 
		
		if (result.getStatus() == RequestStatusEnum.OK.code()) {
			Date dataAtual = new Date(System.currentTimeMillis());
			cardapioDaSemana = result.getMsg();
			
			//Salva nas preferências locais o cardápio da semana
			SharedPreferences.Editor editor = settings.edit();
			editor.putLong(AppConstants.ULTIMA_ATUALIZACAO_CARDAPIO, dataAtual.getTime());
			editor.putString(AppConstants.CARDAPIO_JSON, cardapioDaSemana);
			editor.commit();
			pd.dismiss();
		} else {
			pd.dismiss();
			Toast.makeText(
					mActivity,
					"Não foi possível buscar o cardápio do dia. Verifique sua conexão e tente novamente",
					Toast.LENGTH_LONG).show();
		}
		
		//Abre a activity de exibição do cardápio
		Intent intent = new Intent(mActivity.getApplicationContext(),
				CardapioActivity.class);
		intent.putExtra(AppConstants.CARDAPIO_JSON, cardapioDaSemana);
		mActivity.startActivity(intent);
		mActivity.finish();
	}

}
