		package com.bora_bandeco;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.bora_bandeco.dto.Cardapio;
import com.bora_bandeco.dto.CardapioDaSemana;
import com.google.gson.Gson;

public class CardapioActivity extends ActionBarActivity {
	
	protected CardapioDaSemana cardapioDaSemana;
	protected Cardapio cardapioAtual;
	
	MyAdapter mAdapter;
    ViewPager mPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_cardapio);
		
		ActionBar actionBar = getSupportActionBar();
	    actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME
	        | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);
	    
		String json = getIntent().getExtras().getString(AppConstants.CARDAPIO_JSON);
		
		cardapioDaSemana = (CardapioDaSemana) new Gson().fromJson(json, CardapioDaSemana.class);
		cardapioAtual = cardapioDaSemana.getCardapioAtual();
		
		
		mAdapter = new MyAdapter(getSupportFragmentManager(), cardapioDaSemana);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mPager.setCurrentItem(cardapioDaSemana.getPosicaoCardapioAtual());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.cardapio, menu);
	    
	    return super.onCreateOptionsMenu(menu);
	}

	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.novo_convite:
				Intent intent = new Intent(getApplicationContext(),
						NovoConviteActivity.class);
				startActivity(intent);
				break;
		    
	    }
	    
	    return true;
	}
	
	public static class MyAdapter extends FragmentPagerAdapter {
		CardapioDaSemana cardapioDaSemana;
		
        public MyAdapter(FragmentManager fragmentManager, CardapioDaSemana cardapioDaSemana) {
            super(fragmentManager);
            this.cardapioDaSemana = cardapioDaSemana;
        }
 
        @Override
        public int getCount() {
            return cardapioDaSemana.cardapios.size();
        }
 
        @Override
        public Fragment getItem(int position) {
        	Cardapio cardapio = cardapioDaSemana.cardapios.get(position);
            
            Gson gson = new Gson();
            String jsonCardapioAtual = gson.toJson(cardapio);
            return CardapioFragment.init(jsonCardapioAtual);
        }
    }
}
