package com.bora_bandeco.dto;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Cardapio {
	
	public static String ALMOCO = "ALMOÇO";
	public static String JANTAR = "JANTAR";
	
	public String tipo;
    public String data;
    public String prato_principal;
    public String guarnicao;
    public String pts;
    public String salada;
    public String sobremesa;
    public String suco;
    public String vegetariano;
    
    public Date getData() {
        Date startDate = null;
        try {
        	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            startDate = df.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate; 
    }
    
    public String getDiaSemana() {
    	Calendar c = Calendar.getInstance();
    	c.setTime(getData());
    	   
    	String diaSemana = c.get(Calendar.DAY_OF_WEEK) + "ª-feira";

        return diaSemana; 
    }
}
