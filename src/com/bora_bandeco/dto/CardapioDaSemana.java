package com.bora_bandeco.dto;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CardapioDaSemana {
	public List<Cardapio> cardapios;

	public Cardapio getCardapioAtual() {
		Date dataAtual = new Date(System.currentTimeMillis());
		Calendar calAtual = Calendar.getInstance();
		calAtual.setTime(dataAtual);
		
		for (Cardapio c : cardapios) {
			Calendar calCardapio = Calendar.getInstance();
			calCardapio.setTime(c.getData());
			
			boolean mesmoDia = calAtual.get(Calendar.YEAR) == calCardapio.get(Calendar.YEAR) &&
							   calAtual.get(Calendar.DAY_OF_YEAR) == calCardapio.get(Calendar.DAY_OF_YEAR);
			
			
			if (mesmoDia) {
				if (c.tipo.equals(Cardapio.ALMOCO) && 
					calAtual.get(Calendar.HOUR_OF_DAY) < 14) {
					return c;
				} else if (c.tipo.equals(Cardapio.JANTAR) && 
						calAtual.get(Calendar.HOUR_OF_DAY) >= 14) {
					return c;
				}
			}
		}
		
		//Se o cardápio do dia de hoje não está na lista, buscar o próximo cardápio ou exibir o último 
		for (int i = 0; i < cardapios.size(); i++) {
			if (cardapios.get(i).getData().after(dataAtual)) {
				return cardapios.get(i); 
			}
		}
		return cardapios.get(cardapios.size()-1);
	}
	
	public Cardapio getCardapioPorData(Date data, String tipo) {
		Calendar calAtual = Calendar.getInstance();
		calAtual.setTime(data);
		
		for (Cardapio c : cardapios) {
			Calendar calCardapio = Calendar.getInstance();
			calCardapio.setTime(c.getData());
			
			boolean mesmoDia = calAtual.get(Calendar.YEAR) == calCardapio.get(Calendar.YEAR) &&
					   calAtual.get(Calendar.DAY_OF_YEAR) == calCardapio.get(Calendar.DAY_OF_YEAR);
			if (mesmoDia && c.tipo.equals(tipo)) {
				return c;
			}
		}
		
		return null;
	}
	
	public int getPosicaoCardapioAtual() {
		Date dataAtual = new Date(System.currentTimeMillis());
		Calendar calAtual = Calendar.getInstance();
		calAtual.setTime(dataAtual);
		
		int i = 0;
		for (Cardapio c : cardapios) {
			Calendar calCardapio = Calendar.getInstance();
			calCardapio.setTime(c.getData());
			
			boolean mesmoDia = calAtual.get(Calendar.YEAR) == calCardapio.get(Calendar.YEAR) &&
							   calAtual.get(Calendar.DAY_OF_YEAR) == calCardapio.get(Calendar.DAY_OF_YEAR);
			
			
			if (mesmoDia) {
				if (c.tipo.equals(Cardapio.ALMOCO) && 
					calAtual.get(Calendar.HOUR_OF_DAY) < 14) {
					return i;
				} else if (c.tipo.equals(Cardapio.JANTAR) && 
						calAtual.get(Calendar.HOUR_OF_DAY) >= 14) {
					return i;
				}
			}
			i++;
		}
		
		//Se o cardápio do dia de hoje não está na lista, buscar o próximo cardápio ou exibir o último 
		for (i = 0; i < cardapios.size(); i++) {
			if (cardapios.get(i).getData().after(dataAtual)) {
				return i; 
			}
		}
		return cardapios.size()-1;
	}
}
