package com.bora_bandeco.request;

/**
 * The Class RequestDTO.
 */
public class RequestDTO {

	/** The status of the request, according to CodeEnum.java, used internally */
	private int status;

	/** The msg returned in the request (it can be null or empty). */
	private String msg;

	/**
	 * The error code returned in the request (it can be null or empty),
	 * according to ErrorCodeEnum.java .
	 */
	private String code;

	/**
	 * Instantiates a new request dto.
	 */
	public RequestDTO() {
		// empty constructor
	}

	/**
	 * Instantiates a new request dto.
	 * 
	 * @param status
	 *            the request status used internally
	 * @param msg
	 *            the message
	 * @param code
	 *            the code returned by EvoluCard
	 */
	public RequestDTO(int status, String msg, String code) {
		super();
		this.status = status;
		this.msg = msg;
		this.code = code;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Gets the msg.
	 * 
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Sets the msg.
	 * 
	 * @param msg
	 *            the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code
	 *            the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
